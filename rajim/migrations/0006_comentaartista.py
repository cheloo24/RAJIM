# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('rajim', '0005_comentadisco_id_disco'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComentaArtista',
            fields=[
                ('id_a', models.AutoField(serialize=False, primary_key=True, db_column='ID_A')),
                ('titulo', models.CharField(max_length=40, null=True, db_column='TITULO', blank=True)),
                ('comentario', models.TextField(max_length=200, null=True, db_column='COMENTARIO', blank=True)),
                ('id_art', models.ForeignKey(db_column='ID_ARTISTA', blank=True, to='rajim.Artista', null=True)),
                ('user', models.ForeignKey(db_column='ID_USER', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
